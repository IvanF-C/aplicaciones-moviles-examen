package com.example.examen;

import java.util.ArrayList;

public class PeticionListaProcesadores {
    public String estado;
    public String detalle;
    public ArrayList<ArregloProcesadores> procesadores;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public ArrayList<ArregloProcesadores> getProcesadores() {
        return procesadores;
    }

    public void setProcesadores(ArrayList<ArregloProcesadores> procesadores) {
        this.procesadores = procesadores;
    }
}
