package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Registro extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        Button btnGuardar = (Button) findViewById(R.id.btnCreaRegistro);
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText nameUsuario=(EditText) findViewById(R.id.etRegistroUsuario);
                EditText cont=(EditText) findViewById(R.id.etRegistroContrasena);
                EditText cont2=(EditText) findViewById(R.id.etRegistroRContrasena);

                String usuarionName =nameUsuario.getText().toString();
                String contrasena=cont.getText().toString();
                String contrasena2=cont2.getText().toString();



                if(TextUtils.isEmpty(usuarionName))
                {
                    nameUsuario.setError("Porfavor, Ingrese un Nombre de Usuario");
                    nameUsuario.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(contrasena))
                {
                    cont.setError("Por favor, Ingrese la Contraseña");
                    cont.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(contrasena2))
                {
                    cont2.setError("Por favor, Repita su Contaseña ");
                    cont2.requestFocus();
                    return;
                }

                //verificar que las contraseñas sean iguales

                if (!contrasena.equals(contrasena2))
                {
                    cont.setError("Las Contraseñas no Coinciden");
                    cont.requestFocus();
                    cont2.setError("Las Contraseñas no Coinciden");
                    cont2.requestFocus();
                    return;
                }


                ServicioPeticion service=API.getApi(Registro.this).create(ServicioPeticion.class);
                Call<RegistroUsuarios>registroUsuariosCall=service.registroUsuario(nameUsuario.getText().toString(),cont.getText().toString());
                registroUsuariosCall.enqueue(new Callback<RegistroUsuarios>() {
                    @Override
                    public void onResponse(Call<RegistroUsuarios> call, Response<RegistroUsuarios> response) {
                        RegistroUsuarios peticion=response.body();
                        if (response.body()==null)
                        {
                            Toast.makeText(Registro.this,"Se Presento un Error",Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (peticion.estado=="true")
                        {
                            startActivity(new Intent(Registro.this,MainActivity.class));
                            Toast.makeText(Registro.this,"Registro Exitoso",Toast.LENGTH_SHORT).show();
                        }

                        else
                        {
                            Toast.makeText(Registro.this,"El Error Fue : " + peticion.detalle,Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<RegistroUsuarios> call, Throwable t) {
                        Toast.makeText(Registro.this,"Ocurrio un Error",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }

    public void RegresarInicio(View view)
    {
        Intent Inicio=new Intent( Registro.this,MainActivity.class);
        startActivity(Inicio);
    }


}
