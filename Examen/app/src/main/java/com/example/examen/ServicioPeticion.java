package com.example.examen;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<RegistroUsuarios>registroUsuario(@Field("username")String correo,@Field("password") String contrasena);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Login>loginUsuario(@Field("username")String nombreUsuario,@Field("password") String contrasena);


    @POST("api/informacionProcesadores")
    Call<Menu> listaDetallesProcesadores();

    /*
    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<>IdProcesadores(@Field("username")String procesadorId);
    */



}





