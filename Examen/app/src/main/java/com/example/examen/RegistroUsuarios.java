package com.example.examen;

public class RegistroUsuarios {

    public String correo;
    public String contrasena;
    public String detalle;
    public String estado;

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public RegistroUsuarios(String correo, String contrasena)
    {
        this.correo=correo;
        this.contrasena=contrasena;
    }

}
