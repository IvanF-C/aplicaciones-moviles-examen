package com.example.examen;

public class Login {
    public String usuario;
    public String contrasena;
    public String token;
    public String datalles;
    public String nombreUsuario;
    public String estado;


    public Login(String nombreUsuario,String contrasena)
    {
        this.nombreUsuario = nombreUsuario;
        this.contrasena=contrasena;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getDatalles() {
        return datalles;
    }

    public void setDatalles(String datalles) {
        this.datalles = datalles;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
