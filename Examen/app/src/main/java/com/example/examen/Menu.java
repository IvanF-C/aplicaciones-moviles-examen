package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Menu extends AppCompatActivity {

    ArrayList<String> ListaCompletaID=new ArrayList<>();
    ArrayList<String> ListaCompletaNombre=new ArrayList<>();
    ArrayList<String> ListaCompletaNucleos=new ArrayList<>();
    ArrayList<String> ListaCompletaFrecuencia=new ArrayList<>();

    public ListView listaProcesadores;
    public TextView idProcesador,nombreProcesador,frecuenciaprocesador,nucleoprocesador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        idProcesador=(TextView) findViewById(R.id.tvIdProcesador);
        nombreProcesador=(TextView) findViewById(R.id.tvNombreProcesador);
        frecuenciaprocesador=(TextView) findViewById(R.id.tvNucleoProcesador);
        nucleoprocesador=(TextView) findViewById(R.id.tvNucleoProcesador);
        listaProcesadores=(ListView)findViewById(R.id.lvListaProcesadores);

        final ArrayAdapter arrayAdapter=new ArrayAdapter(this,R.layout.activity_menu,ListaCompletaID);

        listaProcesadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                idProcesador.setText("Id: " + listaProcesadores.getItemAtPosition(position));
                nombreProcesador.setText("Nombre del Procesador: " + listaProcesadores.getItemAtPosition(position));
                frecuenciaprocesador.setText("Frecuencia del Procesador: " + listaProcesadores.getItemAtPosition(position));
                nucleoprocesador.setText("Nucleo del Procesador: " + listaProcesadores.getItemAtPosition(position));
            }
        });

        ServicioPeticion servicioPeticion=API.getApi(Menu.this).create(ServicioPeticion.class);



    }
}
