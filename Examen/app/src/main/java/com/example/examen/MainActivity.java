package com.example.examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    public String APItoken;
    public String NombreUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Button btnInicioSecion=(Button) findViewById(R.id.btnInicio);
        btnInicioSecion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText EditTextUsuario=(EditText) findViewById(R.id.etUsuario);
                EditText EditTextContrasena=(EditText) findViewById(R.id.etContrasena);
                String usuario=EditTextUsuario.getText().toString();
                String contrasena=EditTextContrasena.getText().toString();

                if (TextUtils.isEmpty(usuario))
                {
                    EditTextUsuario.setError("Usuario no Ingresado");
                    EditTextUsuario.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(contrasena))
                {
                    EditTextContrasena.setError("Contraseña no Ingresada");
                    EditTextContrasena.requestFocus();
                    return;
                }

                ServicioPeticion service =API.getApi(MainActivity.this).create(ServicioPeticion.class);
                Call<Login>loginCall=service.loginUsuario(EditTextUsuario.getText().toString(),EditTextContrasena.getText().toString());
                loginCall.enqueue(new Callback<Login>() {
                    @Override
                    public void onResponse(Call<Login> call, Response<Login> response) {
                        Login login=response.body();

                        if (response.body()==null)
                        {
                            Toast.makeText(MainActivity.this,"Error",Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (login.estado=="true")
                        {
                            APItoken=login.token;
                            NombreUsuarios=login.usuario;

                            GuardarDescripcion();
                            Toast.makeText(MainActivity.this,"Seccion Iniciada, BIENVENIDO : " + NombreUsuarios,Toast.LENGTH_LONG).show();
                            Intent IraPanel = new Intent(MainActivity.this,Menu.class);
                            IraPanel.putExtra("username",NombreUsuarios);
                            startActivity(IraPanel);

                        }
                        else
                        {
                            Toast.makeText(MainActivity.this,login.datalles,Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Login> call, Throwable t) {
                        Toast.makeText(MainActivity.this,"Ocurrio Un Error ",Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }
    public void Regristros(View view)
    {
        Intent Registro =new Intent(MainActivity.this,Registro.class);
        startActivity(Registro);
    }





    public void GuardarDescripcion()
    {
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=APItoken;
        SharedPreferences.Editor editor=preferencias.edit();
        editor.putString("Token :",token);
        editor.commit();
    }




}
